const axios = require('axios');

const avto = {
    znamka: 'Audi',
    model: 'A4',
    letnik: 2020,
    cena: 50000
};

// axios.post('http://localhost:5000/api/avtomobili', avto)
//     .then((response) => {
//         console.log(`Avto je bil uspešno vstavljen in je dostopen na ${response.headers.location}`);
//     }).catch(error => {
//         console.log('Napaka: ' + error);
//     })

axios.get('http://localhost:5000/api/avtomobili')
    .then((response) =>  {
        console.log(response.data);
    }).catch(error => {
        console.log('Napaka: ' + error);
    })